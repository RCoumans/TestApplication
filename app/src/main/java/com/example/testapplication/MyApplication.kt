package com.example.testapplication

import android.app.Application
import com.example.testapplication.data.network.MyTribuApi
import com.example.testapplication.data.network.NetworkConnectionInterceptor
import com.example.testapplication.data.repository.PictureRepository
import com.example.testapplication.ui.home.listing.ListingViewModelFactory
import com.example.testapplication.ui.home.upload.UploadViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class MyApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MyApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyTribuApi() }

        bind() from singleton { PictureRepository(instance()) }

        bind() from singleton { UploadViewModelFactory(instance()) }
        bind() from singleton { ListingViewModelFactory(instance()) }
    }
}