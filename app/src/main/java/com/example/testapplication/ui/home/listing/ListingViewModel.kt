package com.example.testapplication.ui.home.listing

import androidx.lifecycle.ViewModel
import com.example.testapplication.data.network.responses.PicturesResponse
import com.example.testapplication.data.repository.PictureRepository

class ListingViewModel(
    private val repository: PictureRepository
) : ViewModel() {

    suspend fun getPictures(): PicturesResponse {
        return repository.getPictures()
    }

}