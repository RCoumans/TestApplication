package com.example.testapplication.ui.home.listing

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testapplication.data.repository.PictureRepository

@Suppress("UNCHECKED_CAST")
class ListingViewModelFactory (
    private val repository: PictureRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListingViewModel(repository) as T
    }
}