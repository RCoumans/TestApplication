package com.example.testapplication.ui.home.upload

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testapplication.data.repository.PictureRepository

@Suppress("UNCHECKED_CAST")
class UploadViewModelFactory (
    private val repository: PictureRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UploadViewModel(repository) as T
    }
}