package com.example.testapplication.ui.home.listing

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testapplication.R
import com.example.testapplication.databinding.ListingFragmentBinding
import com.example.testapplication.util.Coroutines
import com.example.testapplication.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class ListingFragment : Fragment(), KodeinAware {

    //------ Data Initialization ------//

    override val kodein by kodein()
    private val factory: ListingViewModelFactory by instance()

    private lateinit var binding: ListingFragmentBinding
    private lateinit var viewModel: ListingViewModel

    private var imgUrl: ArrayList<String> = ArrayList()
    private lateinit var mAdapter: ListingAdapter
    private var interval = 3
    private var index = 0

    //------ Activity Lifecycle ------//

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.listing_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(ListingViewModel::class.java)
        binding.lifecycleOwner = this

        bindUI()
        bindControl()

        return binding.root
    }

    //------ Bind Graphic Elements ------//

    private fun bindUI() {

        val manager = LinearLayoutManager(requireContext())
        binding.recyclerview.layoutManager = manager
        mAdapter = ListingAdapter(imgUrl, requireContext())
        binding.recyclerview.adapter = mAdapter

        Coroutines.main{
            try {
                val picResponse = viewModel.getPictures()
                imgUrl = picResponse.files as ArrayList<String>

                refreshPictures()

            } catch (e: Exception) {
                binding.rootLayout.snackbar(e.message.toString())
            }
        }
    }

    //------ Bind Control Elements ------//

    private fun bindControl() {
        binding.recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    Log.d("fetch : ", "fetch")
                    refreshPictures()
                }
            }
        })
    }

    //------ Tools ------//

    private fun refreshPictures() {
        if (index + interval < imgUrl.size-1) {
            for (i in 1..interval) {
                mAdapter.addImage(imgUrl[index])

                Log.d("interval(if) : ", interval.toString())
                Log.d("index(if) : ", index.toString())
                Log.d("imgUrl.size(if) : ", imgUrl.size.toString())
                Log.d("--------------- : ", "-------------------")
                ++index
            }
        } else {
            interval = imgUrl.size - index
            for (i in 1..interval) {
                mAdapter.addImage(imgUrl[index])
                Log.d("interval(else) : ", interval.toString())
                Log.d("index(else) : ", index.toString())
                Log.d("imgUrl.size(else) : ", imgUrl.size.toString())
                Log.d("--------------- : ", "-------------------")
                ++index
            }
        }
    }
}