package com.example.testapplication.ui.home.upload

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.testapplication.R
import com.example.testapplication.data.network.MyTribuApi
import com.example.testapplication.data.network.responses.UploadResponse
import com.example.testapplication.databinding.UploadFragmentBinding
import com.example.testapplication.util.getFileName
import com.example.testapplication.util.snackbar
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class UploadFragment : Fragment(), UploadRequestBody.UploadCallback, KodeinAware {

    //------ Data Initialization ------//

    override val kodein by kodein()
    private val factory: UploadViewModelFactory by instance()

    private lateinit var binding: UploadFragmentBinding
    private lateinit var viewModel: UploadViewModel

    //------ Variable ------//

    private var selectedImageUri: Uri? = null

    companion object {
        const val REQUEST_CODE_PICK_IMAGE = 101
    }

    //------ Activity Lifecycle ------//

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.upload_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(UploadViewModel::class.java)
        binding.lifecycleOwner = this

        bindControl()

        return binding.root
    }

    //------ Bind Control Elements ------//

    private fun bindControl() {
        binding.imageView.setOnClickListener {
            openImageChooser()
        }
        binding.buttonUpload.setOnClickListener {
            uploadImage()
        }
    }

    //------ Tools ------//

    private fun openImageChooser() {
        Intent(Intent.ACTION_PICK).also {
            it.type = "image/*"
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            it.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(it, REQUEST_CODE_PICK_IMAGE)
        }
    }

    private fun uploadImage() {
        val parcelFileDescriptor = requireActivity().contentResolver.openFileDescriptor(
            selectedImageUri!!,
            "r",
            null
        ) ?: return

        val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
        val file = File(
            requireActivity().cacheDir, requireActivity().contentResolver.getFileName(
                selectedImageUri!!
            )
        )
        val outputStream = FileOutputStream(file)
        inputStream.copyTo(outputStream)

        binding.progressBar.visibility = VISIBLE
        binding.progressBar.progress = 0
        val body = UploadRequestBody(file, "image", this)

        MyTribuApi().uploadImage(
            MultipartBody.Part.createFormData(
                "file",
                file.name,
                body
            ),
            RequestBody.create(MediaType.parse("multipart/form-data"), "json")
        ).enqueue(object : Callback<UploadResponse> {
            override fun onFailure(call: Call<UploadResponse>, t: Throwable) {
                binding.rootLayout.snackbar(t.message!!)
                binding.progressBar.progress = 0
            }

            override fun onResponse(
                call: Call<UploadResponse>,
                response: Response<UploadResponse>
            ) {
                response.body()?.let {
                    binding.rootLayout.snackbar(it.message)
                    binding.progressBar.progress = 100
                    selectedImageUri = null
                    binding.buttonUpload.isEnabled = false

                    Handler().postDelayed(
                        {
                            binding.progressBar.progress = 0
                            binding.progressBar.visibility = INVISIBLE
                        },
                        3000
                    )
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_PICK_IMAGE -> {
                    Log.d("image1", data?.data.toString())
                    selectedImageUri = data?.data
                    val bitmap: Bitmap =
                        MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, selectedImageUri)
                    binding.imageView.setImageBitmap(bitmap)
                    binding.buttonUpload.isEnabled = true
                }
            }
        }
    }

    override fun onProgressUpdate(percentage: Int) {}
}