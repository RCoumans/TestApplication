package com.example.testapplication.ui.home.listing

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.testapplication.R


class ListingAdapter(var urls: ArrayList<String>, context_: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var context: Context = context_

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        private val image: ImageView = v.findViewById(R.id.image_view) as ImageView
        fun getImage(): ImageView {
            return image
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.listing_item_image, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        getImage(holder.itemView).layout(0,0,0,0)
        Glide.with(context)
            .load(urls[position])
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(getImage(holder.itemView))
    }

    override fun getItemCount(): Int {
        return urls.size
    }

    fun addImage(image : String) {
        urls.add(image)
        this.notifyItemRangeChanged(0, this.itemCount);
    }

//    private fun getImage(v: View) : ImageView{
//        return v.findViewById(R.id.image_view) as ImageView
//    }

    private fun getImage(v: View) : ImageView {
        val image: ImageView = v.findViewById(R.id.image_view) as ImageView
        image.layout(0,0,0,0)
        return image
    }
}

