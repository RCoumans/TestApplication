package com.example.testapplication.data.repository

import com.example.testapplication.data.network.MyTribuApi
import com.example.testapplication.data.network.SafeApiRequest
import com.example.testapplication.data.network.responses.PicturesResponse

class PictureRepository (
    private val api: MyTribuApi,
) : SafeApiRequest() {

    suspend fun getPictures(): PicturesResponse {
        return apiRequest { api.getPictures() }
    }

}
