package com.example.testapplication.data.network

import com.example.testapplication.data.network.responses.PicturesResponse
import com.example.testapplication.data.network.responses.UploadResponse
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface MyTribuApi {

    @Multipart
    @POST("pic-upload")
    fun uploadImage(
        @Part file: MultipartBody.Part,
        @Part("desc") desc: RequestBody
    ): Call<UploadResponse>

    @GET("pics")
    suspend fun getPictures(): Response<PicturesResponse>

    companion object {
        operator fun invoke(): MyTribuApi {
            return Retrofit.Builder()
                .baseUrl("https://demo.mytribunews.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyTribuApi::class.java)
        }
    }
}