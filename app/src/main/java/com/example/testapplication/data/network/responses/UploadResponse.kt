package com.example.testapplication.data.network.responses

data class UploadResponse(
    val error: Boolean,
    val message: String,
    val image: String
)