package com.example.testapplication.data.network.responses

data class PicturesResponse(
    val status: String?,
    val code: String?,
    val message: String?,
    val files: List<String>?
)